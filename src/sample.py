import math
import sys


def double_to_hex(double_value, use_le):
    # tag::doubleToHex[]
    import struct

    fmt = '<d' if use_le else '>d'
    byte_array = struct.pack(fmt, double_value).hex().lstrip('0')
    hex_value = byte_array if byte_array else '0'
    # end::doubleToHex[]
    return hex_value


def hex_to_double(hex_value, use_le):
    # tag::hexToDouble[]
    import struct

    fmt = '<d' if use_le else '>d'
    double_value = struct.unpack(fmt, bytes.fromhex(hex_value.zfill(16)))[0]
    # end::hexToDouble[]
    return double_value

args_map = {}
for arg in sys.argv:
    parts = arg.split('=')
    if len(parts) < 2:
        args_map[parts[0]] = ''
    else:
        args_map[parts[0]] = parts[1]

with open(args_map['--file']) as f:
    for line in f:
        (endianness, double_value, hex_value) = line.strip().split('\t')
        use_le = endianness == 'LE'
        print(endianness, ': ', end='')

        double_value = float(double_value.replace("∞", "inf"))

        print(double_value, '-> ', end='')
        calculated_hex = double_to_hex(double_value, use_le)
        print(calculated_hex, ', ', sep='', end='')

        print(hex_value, '-> ', end='')
        calculated_double = hex_to_double(hex_value, use_le)
        print(calculated_double, ' ', end='')

        if ((double_value == calculated_double) \
            or (math.isnan(double_value) and math.isnan(calculated_double))) \
           and hex_value == calculated_hex:
            print('[ok]')
        else:
            print('[fail]')
            sys.exit(-1)

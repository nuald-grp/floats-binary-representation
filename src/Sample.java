import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

// tag::imports[]
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
// end::imports[]

public class Sample {

    private static String doubleToHex(final double value, boolean useLe) {
        // tag::doubleToHex[]
        final ByteOrder bo = useLe ?
            ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN;
        final byte[] bytes = ByteBuffer.allocate(8).order(bo)
            .putDouble(value).array();
        final long lng = ByteBuffer.wrap(bytes).getLong();
        final String hex = Long.toHexString(lng);
        // end::doubleToHex[]
        return hex;
    }

    private static double hexToDouble(final String hex, boolean useLe) {
        // tag::hexToDouble[]
        final ByteOrder bo = useLe ?
            ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN;
        long longValue = -1;

        final int size = hex.length();
        if (size <= 1) {
            longValue = Long.parseLong(hex, 16);
        } else {
            final long left = Long.parseLong(hex.substring(0, size - 1), 16);
            final long right = Long.parseLong(hex.substring(size - 1), 16);
            longValue = left * 16 + right;
        }

        final byte[] bytes = ByteBuffer.allocate(8)
            .putLong(longValue).array();
        final double value = ByteBuffer.wrap(bytes).order(bo).getDouble();
        // end::hexToDouble[]
        return value;
    }

    private static double modernHexToDouble(final String hex, boolean useLe) {
        // tag::modernHexToDouble[]
        final ByteOrder bo = useLe ?
            ByteOrder.LITTLE_ENDIAN : ByteOrder.BIG_ENDIAN;
        long longValue = Long.parseUnsignedLong(hex, 16);
        final byte[] bytes = ByteBuffer.allocate(8)
            .putLong(longValue).array();
        final double value = ByteBuffer.wrap(bytes).order(bo).getDouble();
        // end::modernHexToDouble[]
        return value;
    }

    public static void main(String[] args) {
        final Map<String, String> argsMap = new HashMap<>();
        for (final String arg: args) {
            String[] parts = arg.split("=");
            if (parts.length < 2) {
                argsMap.put(parts[0], "");
            } else {
                argsMap.put(parts[0], parts[1]);
            }
        }

        final boolean useModern = argsMap.containsKey("--modern");
        if (useModern) {
            System.out.println("Using modern Java");
        }

        BufferedReader reader = null;
        try {
            reader = new BufferedReader(new FileReader(argsMap.get("--file")));
            String line = reader.readLine();
            while (line != null) {
                final String[] result = line.split("\\t");
                final String endianness = result[0];
                final String doubleValueStr = result[1].replace("∞", "Infinity");
                final Double doubleValue = Double.valueOf(doubleValueStr);
                final String hexValue = result[2];
                final boolean useLe = endianness.equals("LE");
                System.out.print(String.format("%s: ", endianness));

                System.out.print(String.format("%s -> ", doubleValue));
                final String calculatedHex = doubleToHex(doubleValue, useLe);
                System.out.print(String.format("%s, ", calculatedHex));

                System.out.print(String.format("%s -> ", hexValue));
                final Double calculatedDouble = useModern ?
                    modernHexToDouble(hexValue, useLe) :
                    hexToDouble(hexValue, useLe);
                System.out.print(String.format("%f ", calculatedDouble));

                if (doubleValue.equals(calculatedDouble)
                    && hexValue.equals(calculatedHex)) {
                    System.out.println("[ok]");
                } else {
                    System.out.println("[fail]");
                    System.exit(-1);
                }

                line = reader.readLine();
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
}

use std::collections::HashMap;
use std::io::{BufRead, BufReader};

fn double_to_hex(value: f64, use_le: bool) -> String {
    // tag::doubleToHex[]
    let bytes = if use_le {
        value.to_le_bytes()
    } else {
        value.to_be_bytes()
    };
    let bytes_str: String = bytes.iter()
        .map( |byte| format!("{:02x}", byte))
        .collect();
    let hex_raw = bytes_str.trim_start_matches('0').to_string();
    let hex = if hex_raw.is_empty() {
        "0".to_string()
    } else {
        hex_raw
    };
    // end::doubleToHex[]
    hex
}

fn hex_to_double(hex: &str, use_le: bool) -> f64{
    // tag::hexToDouble[]
    use std::convert::TryInto;

    let bytes_str = format!("{:0>16}", hex);
    let bytes: Vec<u8> = (0..bytes_str.len())
        .step_by(2)
        .map(|i| u8::from_str_radix(&bytes_str[i..i + 2], 16).unwrap())
        .collect();
    let bytes_array = bytes.as_slice().try_into().unwrap();
    let value = if use_le {
        f64::from_le_bytes(bytes_array)
    } else {
        f64::from_be_bytes(bytes_array)
    };
    // end::hexToDouble[]
    value
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args_map: HashMap<String, String> = std::env::args().map(|arg| {
        let parts: Vec<&str> = arg.split("=").collect();
        if parts.len() < 2 {
            (parts[0].to_string(), "".to_string())
        } else {
            (parts[0].to_string(), parts[1].to_string())
        }
    }).collect();
    let filename = &args_map["--file"];
    let file = std::fs::File::open(filename)?;
    for line in BufReader::new(file).lines().map(|l| l.unwrap()) {
        let pairs: Vec<&str> = line.split('\t').collect();
        let endianness = pairs[0];
        let double_value_str = pairs[1].replace("∞", "inf");
        let double_value: f64 = double_value_str.parse()?;
        let hex_value = pairs[2];
        let use_le = endianness == "LE";
        print!("{}: ", endianness);

        print!("{} -> ", double_value_str);
        let calculated_hex = double_to_hex(double_value, use_le);
        print!("{}, ", calculated_hex);

        print!("{} -> ", hex_value);
        let calculated_double = hex_to_double(hex_value, use_le);
        print!("{} ", calculated_double);

        if ((double_value == calculated_double)
            || (double_value.is_nan() && calculated_double.is_nan()))
            && hex_value == calculated_hex {
            println!("[ok]");
        } else {
            println!("[fail]");
            std::process::exit(-1);
        }
    }

    Ok(())
}

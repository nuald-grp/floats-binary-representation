util.doubleToHex = function(value, useLe) {
    // tag::doubleToHex[]
    const buffer = new ArrayBuffer(8);
    const view = new DataView(buffer);
    view.setFloat64(0, value, useLe);
    const hex = view.getBigUint64(0, false).toString(16);
    // end::doubleToHex[]
    return hex;
};

util.hexToDouble = function(hex, useLe) {
    // tag::hexToDouble[]
    const buffer = new ArrayBuffer(8);
    const view = new DataView(buffer);
    view.setBigUint64(0, BigInt("0x" + hex), false);
    const value = view.getFloat64(0, useLe);
    // end::hexToDouble[]
    return value;
};

util.doubleToHex = function(value, useLe) {
    // tag::doubleToHex[]
    var buf = Array(8),
        sign = value < 0 ? 1 : 0,
        off0 = useLe ? 0 : 4,
        off1 = useLe ? 4 : 0,
        writeUint = useLe ? function(val, pos) {
            buf[pos] = val & 255;
            buf[pos + 1] = val >>> 8  & 255;
            buf[pos + 2] = val >>> 16 & 255;
            buf[pos + 3] = val >>> 24;
        } : function(val, pos) {
            buf[pos] = val >>> 24;
            buf[pos + 1] = val >>> 16 & 255;
            buf[pos + 2] = val >>> 8  & 255;
            buf[pos + 3] = val & 255;
        };

    if (sign) {
        value = -value;
    }

    if (value === 0) {
        writeUint(0, off0);
        writeUint(1 / value > 0 ?
                  0 // positive 0
                  : 2147483648, // negative 0
                  off1);
    } else if (isNaN(value)) {
        writeUint(0, off0);
        writeUint(2146959360, off1);
    } else if (value > 1.7976931348623157e+308) { // +-Infinity
        writeUint(0, off0);
        writeUint((sign << 31 | 2146435072) >>> 0, off1);
    } else {
        var mantissa;
        if (value < 2.2250738585072014e-308) { // denormal
            mantissa = value / 5e-324;
            writeUint(mantissa >>> 0, off0);
            writeUint((sign << 31 | mantissa / 4294967296) >>> 0, off1);
        } else {
            var exponent = Math.floor(Math.log(value) / Math.LN2);
            if (exponent === 1024) {
                exponent = 1023;
            }
            mantissa = value * Math.pow(2, -exponent);
            writeUint(mantissa * 4503599627370496 >>> 0, off0);
            writeUint((sign << 31
                       | exponent + 1023 << 20
                       | mantissa * 1048576 & 1048575) >>> 0,
                      off1);
        }
    }

    var hasValues = false, hex = "";
    for (var i = 0; i < 8; i++) {
        var el = buf[i];
        if ((el == 0 && hasValues) || el != 0) {
            var digits = el.toString(16);
            if (hasValues && digits.length < 2) {
                digits = '0' + digits;
            }
            hasValues = true;
            hex += digits;
        }
    }
    if (!hasValues) {
        hex = '0';
    }
    // end::doubleToHex[]
    return hex;
};

util.hexToDouble = function(value, useLe) {
    // tag::hexToDouble[]
    var buf = Array(8),
        padding = 16 - value.length,
        val = Array(padding + 1).join("0") + value,
        off0 = useLe ? 0 : 4,
        off1 = useLe ? 4 : 0,
        readUint = useLe ? function(pos) {
            return (buf[pos]
                    | buf[pos + 1] << 8
                    | buf[pos + 2] << 16
                    | buf[pos + 3] << 24) >>> 0;
        } : function(pos) {
            return (buf[pos ] << 24
                    | buf[pos + 1] << 16
                    | buf[pos + 2] << 8
                    | buf[pos + 3]) >>> 0;
        };

    for (var i = 0; i < 8; i++) {
        buf[i] = parseInt(val.substring(i * 2, i * 2 + 2), 16);
    }

    var lo = readUint(off0),
        hi = readUint(off1),
        sign = (hi >> 31) * 2 + 1,
        exponent = hi >>> 20 & 2047,
        mantissa = 4294967296 * (hi & 1048575) + lo;

    var value = exponent === 2047
        ? mantissa
        ? NaN
        : sign * Infinity
    : exponent === 0 // denormal
        ? sign * 5e-324 * mantissa
        : sign * Math.pow(2, exponent - 1075) * (mantissa + 4503599627370496);
    // end::hexToDouble[]
    return value;
};

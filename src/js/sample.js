const fs = require('fs');
const readline = require('readline');
const path = require('path');

const util = {};

const argsMap = {};
for (let i = 0; i < process.argv.length; i++) {
    const parts = process.argv[i].split('=');
    if (parts.length < 2) {
        argsMap[parts[0]] = '';
    } else {
        argsMap[parts[0]] = parts[1];
    }
}

let filename = 'common.js';
if ('--modern' in argsMap) {
    console.log('Using modern JavaScript');
    filename = 'modern.js';
}
eval(fs.readFileSync(path.join(__dirname, filename)).toString());

const rl = readline.createInterface({
    input: fs.createReadStream(argsMap['--file'])
});

rl.on('line', (line) => {
    let [endianness, doubleValueStr, hexValue] = line.split('\t');
    const useLe = endianness === 'LE';
    process.stdout.write(`${endianness}: `);

    doubleValueStr = doubleValueStr.replace('∞', 'Infinity');
    const doubleValue = Number(doubleValueStr);

    process.stdout.write(`${doubleValueStr} -> `);
    const calculatedHex = util.doubleToHex(doubleValue, useLe);
    process.stdout.write(`${calculatedHex}, `);

    process.stdout.write(`${hexValue} -> `);
    const calculatedDouble = util.hexToDouble(hexValue, useLe);
    process.stdout.write(`${calculatedDouble} `);

    if (((doubleValue === calculatedDouble)
         || (isNaN(doubleValue) && isNaN(calculatedDouble)))
        && hexValue === calculatedHex) {
        console.log('[ok]');
    } else {
        console.log('[fail]');
        process.exit(-1);
    }
});

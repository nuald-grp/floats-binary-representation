<?php

function double_to_hex($value, $use_le) {
    // tag::doubleToHex[]
    $hex = ltrim(bin2hex(pack($use_le ? 'e' : 'E', $value)), '0');
    if (empty($hex)) {
        $hex = '0';
    }
    // end::doubleToHex[]
    return $hex;
}

function hex_to_double($hex, $use_le) {
    // tag::hexToDouble[]
    $padded = str_pad($hex, 16, '0', STR_PAD_LEFT);
    $value = unpack($use_le ? 'e' : 'E', hex2bin($padded))[1];
    // end::hexToDouble[]
    return $value;
}

$args_map = array();
foreach ($argv as $arg) {
    $parts = explode('=', $arg);
    if (count($parts) == 1) {
        $args_map[$parts[0]] = '';
    } else {
        $args_map[$parts[0]] = $parts[1];
    }
}
$filename = $args_map['--file'];

$handle = fopen($filename, 'r');
while (($line = fgets($handle)) !== false) {
    list($endianness, $double_value_str, $hex_value) = explode("\t", trim($line));
    $use_le = $endianness === 'LE';
    echo "$endianness: ";

    switch ($double_value_str) {
    case '+∞': $double_value = +INF; break;
    case '-∞': $double_value = -INF; break;
    case 'NaN': $double_value = NAN; break;
    default: $double_value = floatval($double_value_str);
    }

    echo "$double_value -> ";
    $calculated_hex = double_to_hex($double_value, $use_le);
    echo "$calculated_hex, ";

    echo "$hex_value -> ";
    $calculated_double = hex_to_double($hex_value, $use_le);
    echo "$calculated_double ";

    if ((($double_value === $calculated_double)
         || (is_nan($double_value) && is_nan($calculated_double)))
        && $hex_value === $calculated_hex) {
      echo '[ok]', PHP_EOL;
    } else {
      echo '[fail]', PHP_EOL;
      exit(-1);
    }
}
fclose($handle);
?>

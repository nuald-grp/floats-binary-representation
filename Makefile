# linux/ppc64le
#
PLATFORMS := linux/amd64 \
	linux/386 \
	linux/arm64 \
	linux/s390x \
	linux/arm/v7 \
	linux/arm/v6

docker := env docker

RUNNERS := $(patsubst %,run[%], $(PLATFORMS))

.PHONY: run_all
run_all: $(RUNNERS)

.PHONY: register
register:
	$(docker) run --rm --privileged \
		docker/binfmt:a7996909642ee92942dcd6cff44b9b95f08dad64

.PHONY: $(PLATFORMS)
$(PLATFORMS): | register
	$(docker) buildx build --platform $@ docker/ \
		-o type=docker,name=floats/$@

.PHONY: $(RUNNERS)
$(RUNNERS):: run[%] : %
	$(docker) run -it --rm -v $(shell pwd):/env --platform $^ floats/$^

.PHONY: run_php
run_php:
# tag::run_php[]
	php -f src/sample.php -- --file=fixtures.tsv
# end::run_php[]

.PHONY: run_py
run_py:
# tag::run_py[]
	python3 src/sample.py --file=fixtures.tsv
# end::run_py[]

RUSTC_VERSION := $(shell rustc --version 2>/dev/null)

ifdef RUSTC_VERSION
target/rust_sample: src/sample.rs | target
# tag::compile_rs[]
	rustc src/sample.rs -o target/rust_sample
# end::compile_rs[]

.PHONY: run_rs
run_rs: target/rust_sample
# tag::run_rs[]
	./target/rust_sample --file=fixtures.tsv
# end::run_rs[]
else
run_rs:
	@echo "Rust is not available"
endif

target/sample.jar: src/Sample.java | target
# tag::compile_java[]
	javac src/Sample.java -d target
	jar cvfe target/sample.jar Sample -C target Sample.class
# end::compile_java[]

.PHONY: run_java
run_java: target/sample.jar
# tag::run_java[]
	java -jar target/sample.jar -- --modern --file=fixtures.tsv
	java -jar target/sample.jar -- --file=fixtures.tsv
# end::run_java[]

.PHONY: run_js
run_js:
# tag::run_js[]
	node src/js/sample.js --modern --file=fixtures.tsv
	node src/js/sample.js --file=fixtures.tsv
# end::run_js[]

target/sample: src/sample.C | target
# tag::compile_cpp[]
	clang++ -g -Wall -Wextra -pedantic -Wcast-align \
		src/sample.C -o target/cpp_sample
# end::compile_cpp[]

.PHONY: run_cpp
run_cpp: target/sample
# tag::run_cpp[]
	./target/cpp_sample --file=fixtures.tsv
# end::run_cpp[]

.PHONY: run
run: run_php run_py run_rs run_java run_js run_cpp

target:
	mkdir -p target

clean:
	rm -rf target
